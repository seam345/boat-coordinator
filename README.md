# Skipper 

A set of microservices designed to automate my boat.
All communication between services is over MQTT

## frontroom-down-lights
- Turns lights on when I walk into boat
- Turns lights off when I have left boat
### dependents
- boat-presence
- (outside) front room door contact sensor

## boat-presence
- If a person is detected on the boat, emmit presence = true packet
- If it cannot determine anyone os present in boat, emit presence = unknown packet
- If it knows no one is on boat, emit presence = false packet
### dependents
- (outside) front room motion sensor
- (outside) front room door contact sensor
- (outside) engine room door contact sensor
- [ ] I should consider hatch sensors 
- [ ] I should maybe consider the accelerometer sensor


## Ideas on things todo
- [ ] Rust library that has a type for every Zigbee2MQTT device 

# How to configure shellys
- [ ] {"NAME":"Shelly 2.5","GPIO":[320,1,32,1,224,193,0,0,640,192,608,225,3456,4736],"FLAG":0,"BASE":18}
- [ ] in console
  - [ ] setOption114 1 # sets option114 to 1 to make all switch clicks not change poer
- [ ] configure mqtt
  - [ ] host = local ip of boat-athena
  - [ ] topic = sh<number>
  - [ ] full topic = shelly/%topic%/%prefix%
- [ ] Add backup rule in console
  - [ ] rule1 ON switch1#state=2 DO ruletimer1 1 ENDON ON rules#timer=1 DO Power2 2 ENDON
  - [ ] rule1 ON