let
pkgs = import <nixpkgs> {};
in
  let
    package = { pkgs, stdenv, binary, cargoSha, versionTag, ... }:
      pkgs.rustPlatform.buildRustPackage rec {
        pname = "skipper-${binary}";
        version = "${versionTag}";

        src = builtins.filterSource
                (path: type: type != "directory" || baseNameOf path != "target")
                ./.;
#        src = builtins.fetchTarball "https://gitlab.com/seam345/skipper/-/archive/${binary}-${versionTag}/skipper-${binary}-${versionTag}.tar.gz";

        cargoSha256 = "${cargoSha}";
        cargoBuildFlags = "--bin ${binary}";
        cargoTestFlags = "--bin ${binary}";
      };
  in
    with pkgs; {
      all-lights =
        callPackage package {
          binary = "all_lights";
          versionTag= "v0.1";
          cargoSha = "sha256-SLJ0+5XhXRUCkztk3Zzi3HleYoJkVxu/9Ln9HqK9Ruw=";
        };
      bathroom-down-lights =
        callPackage package {
          binary = "bathroom-down-lights";
          versionTag= "v0.7.0";
          cargoSha = "sha256-JDVKUJDbYp4T+gSysJ120h/neG0M0n9UpnTazMlijtE=";
        };
      bedroom-down-lights =
        callPackage package {
          binary = "bedroom-down-lights";
          versionTag= "v0.2.0";
          cargoSha = "sha256-vbN4ExV63PelwLBqIW0yl1pZqK93T0RVL+0Ri9gHQFc=";
        };
      dinette-down-lights =
        callPackage package {
          binary = "dinette-down-lights";
          versionTag= "v0.6.0";
          cargoSha = "sha256-wEx4iPp21MWV4XCfWqUAyN+AXjtnU/yLhnsB06SyP3Y=";
        };
      engineroom-down-lights =
        callPackage package {
          binary = "engineroom-down-lights";
          versionTag= "v0.2.0";
          cargoSha = "sha256-UZd4mfABbFANG5yI9TcmUOaPm+jAmJ95r54lgd9fAB8=";
        };
      frontroom-down-lights =
        callPackage package {
          binary = "frontroom-down-lights";
          versionTag= "v0.9.0";
          cargoSha = "sha256-7whfhNdKOPsO90CRoPae4Wl8w8t3AtBwj5oF7pUgXYc=";
        };
      kitchen-lights =
        callPackage package {
          binary = "kitchen-lights";
          versionTag= "v0.3.0";
          cargoSha = "sha256-B2P2JRHoc6UrR7HlwGEW3dJRUHBtYMdVyxbSuRssoTk=";
        };
    }
