use rumqttc::{Publish, QoS};
use serde::ser::{Serialize, SerializeStruct, Serializer};
use skipper::global_state::latest_lights_state::{HsColour, LatestLightState};
use skipper::light_controls::LightState;

// pub struct LatestLightStateSnapshotSerlize1LineMapString {
//     pub instant: u64,
//     pub state: LightState,
// }
//
// impl From<&LatestLightState> for LatestLightStateSnapshotSerlize1LineMapString {
//     fn from(val: &LatestLightState) -> Self {
//         LatestLightStateSnapshotSerlize1LineMapString {
//             instant: val.last_state_change.elapsed().as_secs(),
//             state: val.state.clone(),
//         }
//     }
// }

// #[derive(serde::Serialize)]
pub struct LatestLightStateSnapshotSerlize1LineMapString {
    pub last_state_change: u64,
    pub last_heart_beat: u64,
    pub state: LightState,
    pub brightness: u8,
    pub color_temp: f32, // bound between 153-500 todo consider converting to type
    pub color_hsb: HsColour,
}
impl From<&LatestLightState> for LatestLightStateSnapshotSerlize1LineMapString {
    fn from(val: &LatestLightState) -> Self {
        LatestLightStateSnapshotSerlize1LineMapString {
            last_state_change: val.last_state_change.elapsed().as_secs(),
            last_heart_beat: val.last_heart_beat.elapsed().as_secs(),
            state: val.state.clone(),
            brightness: val.brightness,
            color_temp: val.color_temp,
            color_hsb: val.color_hsb.clone(),
        }
    }
}

impl Serialize for LatestLightStateSnapshotSerlize1LineMapString {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&format!("last_state_change: {}, state: {:?}, last_heart_beat {}, brightness: {}, color_temp: {}, color_hsb: {:?}",&self.last_state_change,&self.state, self.last_heart_beat, self.brightness, self.color_temp, self.color_hsb  ))
        // let mut state = serializer.serialize_struct("LatestLightState", 2)?;
        // state.serialize_field("instant", &self.instant)?;
        // state.serialize_field("state", &self.state)?;
        // state.end()
    }
}

//
// #[derive(serde::Serialize)]
// pub struct HsColourSnapshotable {
//     pub hue: u16,       // 0-360
//     pub saturation: u8, // 0-100
// }
// impl From<&HsColour> for HsColourSnapshotable {
//     fn from(val: &HsColour) -> Self {
//         HsColourSnapshotable {
//             hue: val.hue,
//             saturation: val.saturation,
//         }
//     }
// }

pub struct DisplayPacket {
    packet: Publish,
}

impl From<Publish> for DisplayPacket {
    fn from(value: Publish) -> Self {
        DisplayPacket { packet: value }
    }
}

impl Serialize for DisplayPacket {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("Publish", 4)?;
        state.serialize_field("topic", &self.packet.topic)?;
        match &self.packet.qos {
            QoS::AtMostOnce => state.serialize_field("qos", "AtMostOnce")?,
            QoS::AtLeastOnce => state.serialize_field("qos", "AtLeastOnce")?,
            QoS::ExactlyOnce => state.serialize_field("qos", "ExactlyOnce")?,
        }
        state.serialize_field("retain", &self.packet.retain)?;
        state.serialize_field(
            "payload",
            std::str::from_utf8(&self.packet.payload).expect("mqtt payload utf8"),
        )?;
        state.end()
    }
}
