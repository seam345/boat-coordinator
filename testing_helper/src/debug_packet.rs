use crate::snapshotable_types::DisplayPacket;
use log::{debug, info, trace};
use rumqttc::{Publish, QoS};
use serde::Serialize;
use std::future::Future;
use std::sync::Arc;
use tokio::sync::Mutex;

#[derive(Debug)]
pub enum DevicesVec {
    Light,
    TapDial,
    ShellyStatPower,
    ShellyTeleSensor,
    ShellyResult,
}

struct MakeablePublish {
    insta_name: &'static str,
    payload: &'static str,
}

impl From<&MakeablePublish> for Publish {
    fn from(val: &MakeablePublish) -> Self {
        Publish::new("", QoS::AtMostOnce, val.payload)
    }
}
const LIGHT_PAYLOAD_VEC: &[MakeablePublish] = &[
    MakeablePublish {
        insta_name: "LightStateOff",
        payload: r#"
        {
            "brightness": 254,
            "color": {
                "hue":212,"saturation":10,"x":0.3131,"y":0.3232
            },
            "color_mode":"color_temp",
            "color_temp":153,
            "last_seen":"2023-02-17T11:29:23.708Z",
            "linkquality":231,
            "state":"OFF",
            "update":{"state":"available"},
            "update_available":true
        }
    "#,
    },
    MakeablePublish {
        insta_name: "LightStateOn",
        payload: r#"
        {
            "brightness": 254,
            "color": {
                "hue":212,"saturation":10,"x":0.3131,"y":0.3232
            },
            "color_mode":"color_temp",
            "color_temp":153,
            "last_seen":"2023-02-17T11:29:23.708Z",
            "linkquality":231,
            "state":"ON",
            "update":{"state":"available"},
            "update_available":true
        }
    "#,
    },
];

const SHELLY_STAT_POWER_PAYLOAD_VEC: &[MakeablePublish] = &[
    MakeablePublish {
        insta_name: "ShellyOff",
        payload: "OFF",
    },
    MakeablePublish {
        insta_name: "ShellyOn",
        payload: "ON",
    },
];

const SHELLY_TELE_SENSOR_PAYLOAD_VEC: &[MakeablePublish] = &[MakeablePublish {
    insta_name: "ShellySensorCheckinP1OffP2Off",
    payload: r#"{
    "ANALOG": { "Temperature": 40.9 },
    "ENERGY": {
        "ApparentPower": [ 29, 0 ],
        "Current": [ 0.128, 0 ],
        "Factor": [ 0.6, 0 ],
        "Frequency": 50,
        "Period": 1,
        "Power": [ 17, 0 ],
        "ReactivePower": [ 22, 0 ],
        "Today": 0.168,
        "Total": 8.724,
        "TotalStartTime": "2022-12-13T22:18:02",
        "Voltage": 226,
        "Yesterday": 0.42
    },
    "Switch1": "OFF",
    "Switch2": "OFF",
    "TempUnit": "C",
    "Time": "2023-02-22T09:37:38"
}"#,
}];

const SHELLY_RESULT_PAYLOAD_VEC: &[MakeablePublish] = &[MakeablePublish {
    insta_name: "ShellyToggle",
    payload: r#"
    {"Switch1": { "Action": "TOGGLE" } }
    "#,
}];

const TAP_DIAL_PAYOAD_VEC: &[MakeablePublish] = &[
    MakeablePublish {
        insta_name: "Button1press",
        payload: r#"{"battery":100,"linkquality": 255,"action" : "button_1_press"}"#,
    },
    MakeablePublish {
        insta_name: "Button1pressrelease",
        payload: r#"{"battery":100,"linkquality":255,"action":"button_1_press_release"} "#,
    },
    MakeablePublish {
        insta_name: "Button1hold",
        payload: r#"{"battery":100,"linkquality":255,"action":"button_1_hold"}"#,
    },
    MakeablePublish {
        insta_name: "Button1holdrelease",
        payload: r#"{"battery":100,"linkquality":255,"action":"button_1_hold_release"}"#,
    },
    MakeablePublish {
        insta_name: "Button2press",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action": "button_2_press"

                                } "#,
    },
    MakeablePublish {
        insta_name: "Button2pressrelease",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action" : "button_2_press_release"

                                } "#,
    },
    MakeablePublish {
        insta_name: "Button2hold",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action": "button_2_hold"

                                } "#,
    },
    MakeablePublish {
        insta_name: "Button2holdrelease",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action" : "button_2_hold_release"

                                }  "#,
    },
    MakeablePublish {
        insta_name: "Button3press",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action": "button_3_press"

                                }  "#,
    },
    MakeablePublish {
        insta_name: "Button3pressrelease",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action" : "button_3_press_release"

                                }  "#,
    },
    MakeablePublish {
        insta_name: "Button3hold",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action": "button_3_hold"

                                } "#,
    },
    MakeablePublish {
        insta_name: "Button3holdrelease",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action" : "button_3_hold_release"

                                }  "#,
    },
    MakeablePublish {
        insta_name: "Button4press",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action": "button_4_press"

                                } "#,
    },
    MakeablePublish {
        insta_name: "Button4pressrelease",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action" : "button_4_press_release"

                                } "#,
    },
    MakeablePublish {
        insta_name: "Button4hold",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action": "button_4_hold"

                                } "#,
    },
    MakeablePublish {
        insta_name: "Button4holdrelease",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action" : "button_4_hold_release"

                                } "#,
    },
    MakeablePublish {
        insta_name: "Dialrotateleftstep",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action" : "dial_rotate_left_step"

                                } "#,
    },
    MakeablePublish {
        insta_name: "Dialrotateleftslow",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action" : "dial_rotate_left_slow"

                                } "#,
    },
    MakeablePublish {
        insta_name: "Dialrotateleftfast",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action" : "dial_rotate_left_fast"

                                } "#,
    },
    MakeablePublish {
        insta_name: "Dialrotaterightstep",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action" : "dial_rotate_right_step"

                                }  "#,
    },
    MakeablePublish {
        insta_name: "Dialrotaterightslow",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action" : "dial_rotate_right_slow"

                                }  "#,
    },
    MakeablePublish {
        insta_name: "Dialrotaterightfast",
        payload: r#"            {
                                    "battery":100,
                                    "linkquality": 255,
                                    "action" : "dial_rotate_right_fast"

                                } "#,
    },
];

pub async fn loop_over_all_variants<G, GS, I, F, Fut>(
    global_state_generator: fn() -> G,
    device_topic_vec: Vec<(DevicesVec, Option<&'static str>)>,
    info_generator: fn(&Publish, G) -> I,
    mut settings: insta::Settings,
    handle_publish: F,
) where
    I: Serialize,
    F: Fn(Publish, Arc<Mutex<G>>) -> Fut,
    Fut: Future<Output = Vec<Publish>>,
    G: Clone + Into<GS>,
    GS: Serialize,
{
    debug!("asserting all devices are accounted for!");
    // All devices should have been thought about so should be in vec, if we dont want to test a a device we can do (device,None)
    let mut checker = 0u8;
    for device_topic in device_topic_vec.iter() {
        match device_topic.0 {
            DevicesVec::Light => checker |= 1,
            DevicesVec::TapDial => checker |= 2,
            DevicesVec::ShellyTeleSensor => checker |= 4,
            DevicesVec::ShellyResult => checker |= 8,
            DevicesVec::ShellyStatPower => checker |= 16,
        }
    }
    assert_eq!(checker, 31);

    'device_loop: for device_topic in device_topic_vec.iter() {
        if device_topic.1.is_none() {
            trace!(
                "device had None topic skipping check for device type: {:?}",
                device_topic.0
            );
            continue 'device_loop;
        }
        let publish_vec = match device_topic.0 {
            DevicesVec::Light => LIGHT_PAYLOAD_VEC,
            DevicesVec::TapDial => TAP_DIAL_PAYOAD_VEC,
            DevicesVec::ShellyStatPower => SHELLY_STAT_POWER_PAYLOAD_VEC,
            DevicesVec::ShellyTeleSensor => SHELLY_TELE_SENSOR_PAYLOAD_VEC,
            DevicesVec::ShellyResult => SHELLY_RESULT_PAYLOAD_VEC,
        };

        for makable_publish in publish_vec {
            info!("test setup");
            trace!("create global state from generator");
            let global_state = Arc::new(Mutex::new(global_state_generator()));
            trace!("Clone Publish and add correct topic");
            let mut publish: Publish = makable_publish.into();
            publish.topic = device_topic.1.expect("device_topic has topic").to_owned();
            trace!("Set insta settings");
            let info: I = info_generator(&publish, global_state_generator());
            settings.set_info(&info);
            info!("run test");
            let packets = handle_publish(publish, global_state.clone()).await;
            info!("assert received Publishes");
            let value: GS = global_state.lock().await.clone().into();
            settings.bind(|| {
                insta::assert_yaml_snapshot!(format!("{}_{}", &makable_publish.insta_name, device_topic.1.expect("has topic")), value);
            });
            let packet_num = packets.len();
            for packet in packets.into_iter().enumerate() {
                let info2 = InfoOrdered {
                    info: &info,
                    order_num: <usize as TryInto<i32>>::try_into(packet.0).unwrap() + 1,
                };
                settings.set_info(&info2);
                settings.bind(|| {
                    insta::assert_yaml_snapshot!(
                        format!(
                            "{}_{}_SentPacket_{}",
                            &makable_publish.insta_name, device_topic.1.expect("has topic"),
                            <usize as TryInto<i32>>::try_into(packet.0).unwrap() + 1
                        ),
                        <rumqttc::Publish as Into<DisplayPacket>>::into(packet.1)
                    );
                });
            }
            settings.set_info(&info);
            settings.bind(|| {
                insta::assert_debug_snapshot!(
                    format!("{}_{}_packetsSent", &makable_publish.insta_name, device_topic.1.expect("has topic")),
                    packet_num
                );
            });
        }
    }
}

#[derive(serde::Serialize)]
struct InfoOrdered<'a, I>
where
    I: Serialize,
{
    info: &'a I,
    order_num: i32,
}
