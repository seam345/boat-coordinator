use rumqttc::{Publish, QoS};
use serde::ser::SerializeStruct;
use serde::Serializer;

pub fn serialize_publish<S>(value: &Publish, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let mut state = serializer.serialize_struct("Publish", 6)?;
    state.serialize_field("dup", &value.dup)?;
    state.serialize_field(
        "qos",
        &match &value.qos {
            QoS::AtMostOnce => 0u8,
            QoS::AtLeastOnce => 1u8,
            QoS::ExactlyOnce => 2u8,
        },
    )?;
    state.serialize_field("retain", &value.retain)?;
    state.serialize_field("topic", &value.topic)?;
    state.serialize_field("pkid", &value.pkid)?;
    state.serialize_field("payload", std::str::from_utf8(&value.payload).unwrap())?;
    state.end()
}
