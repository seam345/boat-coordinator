extern crate core;

use log::{debug, error, info, trace};
use rumqttc::{Event, SubscribeFilter};
use rumqttc::{Incoming, MqttOptions, Publish, QoS};
use serde_json::json;
use std::collections::HashMap;
use std::sync::Arc;
use tokio::task;
use tokio::time::Duration;
use zigbee2mqtt_types::vendors::philips::Zigbee87195144409378719514440999;
use zigbee2mqtt_types::vendors::philips::Zigbee87195144409378719514440999Action;

use rumqttc::AsyncClient;
use skipper::global_state::latest_lights_state::{
    HasLightsStateHashMap, LatestLightState, NoEntryInHashMap,
};
use skipper::light_controls::{update_light_and_return_if_light_heart_beat, LightState};
use skipper::mqtt_topics::{
    BATHROOM_LIGHT_PORT_BACK, BATHROOM_LIGHT_PORT_FRONT, BATHROOM_LIGHT_STARBOARD_BACK,
    BATHROOM_LIGHT_STARBOARD_FRONT, BATHROOM_LIGHT_STARBOARD_MIDDLE, BEDROOM_LIGHT_PORT_BACK,
    BEDROOM_LIGHT_PORT_FRONT, BEDROOM_LIGHT_STARBOARD_BACK, BEDROOM_LIGHT_STARBOARD_FRONT,
    BEDROOM_LIGHT_STARBOARD_MIDDLE, DINETTE_LIGHT_PORT_BACK, DINETTE_LIGHT_PORT_FRONT,
    DINETTE_LIGHT_STARBOARD_BACK, DINETTE_LIGHT_STARBOARD_FRONT, ENGINEROOM_LIGHT_PORT_BACK,
    ENGINEROOM_LIGHT_STARBOARD_FRONT, FRONTROOM_LIGHT_PORT_BACK, FRONTROOM_LIGHT_PORT_FRONT,
    FRONTROOM_LIGHT_PORT_MIDDLE, FRONTROOM_LIGHT_STARBOARD_BACK, FRONTROOM_LIGHT_STARBOARD_FRONT,
    FRONTROOM_LIGHT_STARBOARD_MIDDLE, KITCHEN_LIGHT_PORT_BACK, KITCHEN_LIGHT_PORT_FRONT,
    KITCHEN_LIGHT_PORT_MIDDLE, KITCHEN_LIGHT_STARBOARD, KITCHEN_LIGHT_STARBOARD_BACK,
};
use skipper::send_packets::publish_all_packets;
use tokio::sync::Mutex;

#[allow(dead_code)]
const ALL_LIGHT_GROUP: &str = "zigbee2mqtt/all_spotlights/set";
const PHILIPS_HUE_DIAL: &str = "zigbee2mqtt/0x001788010d349f91";

const TOPICS: &[&str] = &[
    FRONTROOM_LIGHT_PORT_FRONT,
    FRONTROOM_LIGHT_PORT_MIDDLE,
    FRONTROOM_LIGHT_STARBOARD_BACK,
    FRONTROOM_LIGHT_STARBOARD_FRONT,
    FRONTROOM_LIGHT_STARBOARD_MIDDLE,
    DINETTE_LIGHT_PORT_BACK,
    DINETTE_LIGHT_PORT_FRONT,
    DINETTE_LIGHT_STARBOARD_FRONT,
    DINETTE_LIGHT_STARBOARD_BACK,
    KITCHEN_LIGHT_PORT_BACK,
    KITCHEN_LIGHT_PORT_FRONT,
    KITCHEN_LIGHT_PORT_MIDDLE,
    KITCHEN_LIGHT_STARBOARD,
    KITCHEN_LIGHT_STARBOARD_BACK,
    BATHROOM_LIGHT_PORT_BACK,
    BATHROOM_LIGHT_PORT_FRONT,
    BATHROOM_LIGHT_STARBOARD_BACK,
    BATHROOM_LIGHT_STARBOARD_FRONT,
    BATHROOM_LIGHT_STARBOARD_MIDDLE,
    BEDROOM_LIGHT_PORT_BACK,
    BEDROOM_LIGHT_PORT_FRONT,
    BEDROOM_LIGHT_STARBOARD_BACK,
    BEDROOM_LIGHT_STARBOARD_FRONT,
    BEDROOM_LIGHT_STARBOARD_MIDDLE,
    ENGINEROOM_LIGHT_PORT_BACK,
    ENGINEROOM_LIGHT_STARBOARD_FRONT,
    FRONTROOM_LIGHT_PORT_BACK,
    PHILIPS_HUE_DIAL,
];
const LIGHT_TOPICS: &[&str] = &[
    FRONTROOM_LIGHT_PORT_FRONT,
    FRONTROOM_LIGHT_PORT_MIDDLE,
    FRONTROOM_LIGHT_STARBOARD_BACK,
    FRONTROOM_LIGHT_STARBOARD_FRONT,
    FRONTROOM_LIGHT_STARBOARD_MIDDLE,
    DINETTE_LIGHT_PORT_BACK,
    DINETTE_LIGHT_PORT_FRONT,
    DINETTE_LIGHT_STARBOARD_FRONT,
    DINETTE_LIGHT_STARBOARD_BACK,
    KITCHEN_LIGHT_PORT_BACK,
    KITCHEN_LIGHT_PORT_FRONT,
    KITCHEN_LIGHT_PORT_MIDDLE,
    KITCHEN_LIGHT_STARBOARD,
    KITCHEN_LIGHT_STARBOARD_BACK,
    BATHROOM_LIGHT_PORT_BACK,
    BATHROOM_LIGHT_PORT_FRONT,
    BATHROOM_LIGHT_STARBOARD_BACK,
    BATHROOM_LIGHT_STARBOARD_FRONT,
    BATHROOM_LIGHT_STARBOARD_MIDDLE,
    BEDROOM_LIGHT_PORT_BACK,
    BEDROOM_LIGHT_PORT_FRONT,
    BEDROOM_LIGHT_STARBOARD_BACK,
    BEDROOM_LIGHT_STARBOARD_FRONT,
    BEDROOM_LIGHT_STARBOARD_MIDDLE,
    ENGINEROOM_LIGHT_PORT_BACK,
    ENGINEROOM_LIGHT_STARBOARD_FRONT,
    FRONTROOM_LIGHT_PORT_BACK,
];

#[derive(PartialEq, Debug, Clone)]
enum Mode {
    Brightness,
    Colour,
    Warm,
}

#[derive(Debug, Clone)]
struct DerivedState {
    lights_brightness: u8,
    lights_warmth: f32,
    lights_hue: u16,
    #[allow(dead_code)]
    last_user_input: std::time::Instant,
    current_mode: Mode,
    last_lights_state: HashMap<&'static str, LatestLightState>,
}

impl Default for DerivedState {
    fn default() -> Self {
        DerivedState {
            lights_brightness: 128, // consider 1 as i dont want to be blinded in the night!
            lights_warmth: 153.0,
            lights_hue: 0,
            last_user_input: std::time::Instant::now(),
            current_mode: Mode::Brightness,
            last_lights_state: HashMap::from([
                (FRONTROOM_LIGHT_PORT_FRONT, LatestLightState::default()),
                (FRONTROOM_LIGHT_PORT_MIDDLE, LatestLightState::default()),
                (FRONTROOM_LIGHT_PORT_BACK, LatestLightState::default()),
                (FRONTROOM_LIGHT_STARBOARD_FRONT, LatestLightState::default()),
                (
                    FRONTROOM_LIGHT_STARBOARD_MIDDLE,
                    LatestLightState::default(),
                ),
                (FRONTROOM_LIGHT_STARBOARD_BACK, LatestLightState::default()),
                (DINETTE_LIGHT_STARBOARD_FRONT, LatestLightState::default()),
                (DINETTE_LIGHT_STARBOARD_BACK, LatestLightState::default()),
                (DINETTE_LIGHT_PORT_BACK, LatestLightState::default()),
                (DINETTE_LIGHT_PORT_FRONT, LatestLightState::default()),
                (KITCHEN_LIGHT_STARBOARD_BACK, LatestLightState::default()),
                (KITCHEN_LIGHT_STARBOARD, LatestLightState::default()),
                (KITCHEN_LIGHT_PORT_FRONT, LatestLightState::default()),
                (KITCHEN_LIGHT_PORT_MIDDLE, LatestLightState::default()),
                (KITCHEN_LIGHT_PORT_BACK, LatestLightState::default()),
                (BATHROOM_LIGHT_STARBOARD_MIDDLE, LatestLightState::default()),
                (BATHROOM_LIGHT_STARBOARD_BACK, LatestLightState::default()),
                (BATHROOM_LIGHT_STARBOARD_FRONT, LatestLightState::default()),
                (BATHROOM_LIGHT_PORT_FRONT, LatestLightState::default()),
                (BATHROOM_LIGHT_PORT_BACK, LatestLightState::default()),
                (BEDROOM_LIGHT_STARBOARD_BACK, LatestLightState::default()),
                (BEDROOM_LIGHT_STARBOARD_MIDDLE, LatestLightState::default()),
                (BEDROOM_LIGHT_STARBOARD_FRONT, LatestLightState::default()),
                (BEDROOM_LIGHT_PORT_BACK, LatestLightState::default()),
                (BEDROOM_LIGHT_PORT_FRONT, LatestLightState::default()),
                (ENGINEROOM_LIGHT_PORT_BACK, LatestLightState::default()),
                (
                    ENGINEROOM_LIGHT_STARBOARD_FRONT,
                    LatestLightState::default(),
                ),
            ]),
        }
    }
}

impl HasLightsStateHashMap for DerivedState {
    fn clone_latest_light_state(&self, light: &str) -> Option<LatestLightState> {
        self.last_lights_state.get(light).cloned()
    }

    fn update_latest_light_state(
        &mut self,
        light: &str,
        new_state: LatestLightState,
    ) -> Result<(), NoEntryInHashMap> {
        let light_state = self.last_lights_state.get_mut(light);
        match light_state {
            None => Err(NoEntryInHashMap {
                attempted_key: light.to_string(),
            }),
            Some(current_state) => {
                *current_state = new_state;
                Ok(())
            }
        }
    }
}

const MQTT_CLIENT_ID: &str = "all-lights";
#[tokio::main]
async fn main() {
    // construct a subscriber that prints formatted traces to stdout
    // let subscriber = tracing_subscriber::FmtSubscriber::new();
    // use that subscriber to process traces emitted after this point
    // console_subscriber::init();
    env_logger::init();
    let mut mqttoptions = MqttOptions::new(MQTT_CLIENT_ID, "100.96.69.67", 1883);
    mqttoptions.set_keep_alive(Duration::from_secs(5));

    let (client, mut eventloop) = AsyncClient::new(mqttoptions, 10);
    skipper::mqtt_helpers::confirm_mqtt_connection(MQTT_CLIENT_ID, &mut eventloop)
        .await
        .expect("Connected to broker");

    client
        .subscribe_many(
            TOPICS
                .iter()
                .map(|val| SubscribeFilter {
                    path: val.to_string(),
                    qos: QoS::AtMostOnce,
                })
                .collect::<Vec<SubscribeFilter>>(),
        )
        .await
        .unwrap();
    trace!("Subscribed to {:?}", TOPICS);

    let derived_state = Arc::new(Mutex::new(DerivedState::default()));

    while let Ok(notification) = eventloop.poll().await {
        match notification {
            Event::Incoming(packet) => match packet {
                Incoming::Connect(_) => {}
                Incoming::ConnAck(_) => {}
                Incoming::Publish(publish) => {
                    let cloned_client: AsyncClient = client.clone();
                    let cloned_ephemeral_state = derived_state.clone();
                    let publish = dbg!(publish);
                    task::spawn(async move {
                        let state_changed =
                            handle_publish(publish, cloned_ephemeral_state.clone()).await;
                        if state_changed {
                            let packets = publish_new_light_state(cloned_ephemeral_state).await;
                            publish_all_packets(packets, cloned_client).await;
                        }
                    });
                }
                Incoming::PubAck(_) => {}
                Incoming::PubRec(_) => {}
                Incoming::PubRel(_) => {}
                Incoming::PubComp(_) => {}
                Incoming::Subscribe(_) => {}
                Incoming::SubAck(_) => {}
                Incoming::Unsubscribe(_) => {}
                Incoming::UnsubAck(_) => {}
                Incoming::PingReq => {}
                Incoming::PingResp => {}
                Incoming::Disconnect => {}
            },
            Event::Outgoing(_packet) => {}
        }
    }
}

async fn handle_publish(publish: Publish, derived_state: Arc<Mutex<DerivedState>>) -> bool {
    let mut state_changed = false;
    trace!(
        "packet received: {:?}, packet payload: {:?}, current_state: {:?}",
        publish,
        std::str::from_utf8(&publish.payload).expect("MQTT payload utf8 encoded"),
        &derived_state.lock().await
    );
    if publish.topic == PHILIPS_HUE_DIAL {
        state_changed = handle_tap_dial(publish, derived_state).await;
    } else if LIGHT_TOPICS.iter().any(|val| publish.topic == *val) {
        match update_light_and_return_if_light_heart_beat(&publish, derived_state).await {
            Ok(_) => {}
            Err(_) => {
                todo!()
            }
        }
    }
    state_changed
}

async fn handle_tap_dial(publish: Publish, derived_state: Arc<Mutex<DerivedState>>) -> bool {
    let parsed: Zigbee87195144409378719514440999 =
        match serde_json::from_str(std::str::from_utf8(&publish.payload).unwrap()) {
            Ok(motion_json) => motion_json,
            Err(err) => {
                log::error!(
                    "failed to parse packet sent to handle_tap_dial, packet: {:?}, parse_err: {:?}",
                    publish,
                    err
                );
                return false;
            }
        };
    let mut state_changed = false;
    // dbg!(&parsed);
    match parsed.action {
        Zigbee87195144409378719514440999Action::Dialrotateleftstep => {
            state_changed = amend_state(Change::SmallSub, derived_state.clone()).await;
        }
        Zigbee87195144409378719514440999Action::Dialrotateleftslow => {
            state_changed = amend_state(Change::MediumSub, derived_state.clone()).await;
        }
        Zigbee87195144409378719514440999Action::Dialrotateleftfast => {
            state_changed = amend_state(Change::BigSub, derived_state.clone()).await;
        }
        Zigbee87195144409378719514440999Action::Dialrotaterightstep => {
            state_changed = amend_state(Change::SmallAdd, derived_state.clone()).await;
        }
        Zigbee87195144409378719514440999Action::Dialrotaterightslow => {
            state_changed = amend_state(Change::MediumAdd, derived_state.clone()).await;
        }
        Zigbee87195144409378719514440999Action::Dialrotaterightfast => {
            state_changed = amend_state(Change::BigAdd, derived_state.clone()).await;
        }
        Zigbee87195144409378719514440999Action::Button1press
        | Zigbee87195144409378719514440999Action::Button1hold => {
            if derived_state.lock().await.current_mode == Mode::Colour {
                derived_state.lock().await.current_mode = Mode::Brightness;
            } else {
                derived_state.lock().await.current_mode = Mode::Colour;
            }
        }
        Zigbee87195144409378719514440999Action::Button2press
        | Zigbee87195144409378719514440999Action::Button2hold => {
            if derived_state.lock().await.current_mode == Mode::Warm {
                derived_state.lock().await.current_mode = Mode::Brightness;
            } else {
                derived_state.lock().await.current_mode = Mode::Warm;
            }
        }
        _ => {}
    }
    state_changed
}

enum Change {
    BigAdd,
    MediumAdd,
    SmallAdd,
    BigSub,
    MediumSub,
    SmallSub,
}

async fn amend_state(change: Change, derived_state: Arc<Mutex<DerivedState>>) -> bool {
    let mut state_changed = false;
    let value_change: i8 = match change {
        Change::BigAdd => 10,
        Change::MediumAdd => 5,
        Change::SmallAdd => 1,
        Change::BigSub => -10,
        Change::MediumSub => -5,
        Change::SmallSub => -1,
    };
    // I'm assuming this releases during match ..
    let mode = derived_state.lock().await.current_mode.clone();
    match mode {
        Mode::Brightness => {
            let current_brightness = derived_state.lock().await.lights_brightness;
            let new_brightness = match current_brightness.checked_add_signed(value_change) {
                Some(num) => {
                    if num == 0 {
                        1
                    } else {
                        num
                    }
                }
                None => {
                    if value_change > 0 {
                        255
                    } else {
                        1
                    }
                }
            };
            if current_brightness != new_brightness {
                derived_state.lock().await.lights_brightness = new_brightness;
                state_changed = true;
            }
        }
        Mode::Warm => {
            let current_warmth = derived_state.lock().await.lights_warmth;
            let mut new_warmth = current_warmth + <i8 as Into<f32>>::into(value_change);
            if new_warmth < 153.0 {
                new_warmth = 153.0;
            }
            if new_warmth > 500.0 {
                new_warmth = 500.0;
            }
            if current_warmth != new_warmth {
                derived_state.lock().await.lights_warmth = new_warmth;
                state_changed = true;
            }
        }
        Mode::Colour => {
            let current_hue = derived_state.lock().await.lights_hue;
            // not a huge fan of the + 360 hack
            let new_hue = match (current_hue + 360).checked_add_signed(value_change.into()) {
                Some(num) => {
                    if num > 360 {
                        num - 360
                    } else {
                        num
                    }
                }
                None => {
                    error!("new hue failed for some reason");
                    0
                }
            };
            if current_hue != new_hue {
                derived_state.lock().await.lights_hue = new_hue;
                state_changed = true;
            }
        }
    }
    // trace!("mode: {:?}, bightness: {:?}, hue: {:?}, warmth: {:?}", derived_state.lock().await.current_mode, derived_state.lock().await.lights_brightness, derived_state.lock().await.lights_hue, derived_state.lock().await.lights_warmth);
    state_changed
}

async fn publish_new_light_state(global_state: Arc<Mutex<DerivedState>>) -> Vec<Publish> {
    let mut packets = vec![];
    let mode = global_state.lock().await.current_mode.clone();
    let payload = match mode {
        Mode::Brightness => {
            let brightness = global_state.lock().await.lights_brightness;
            json!({ "brightness": brightness })
        }
        Mode::Colour => {
            let hue = global_state.lock().await.lights_hue;
            json!({"color": {"hue": hue, "saturation": 100}})
        }
        Mode::Warm => {
            let temp = global_state.lock().await.lights_warmth;
            json!({ "color_temp": temp })
        }
    };

    for light_endpoint in LIGHT_TOPICS {
        debug!("getting light {}", light_endpoint);
        let current_state = global_state
            .lock()
            .await
            .clone_latest_light_state(light_endpoint)
            .unwrap_or_else(|| panic!("light ({}) exists", light_endpoint));
        debug!("Got light state: {:?}", &current_state);

        match current_state.state {
            LightState::On => {
                info!(
                    "adding to publish vec Topic: {}/set, payload: {}",
                    light_endpoint,
                    &payload.to_string()
                );
                packets.push(Publish::new(
                    format!("{}/set", light_endpoint),
                    QoS::AtMostOnce,
                    payload.to_string().as_bytes(),
                ));
            }
            LightState::Off => {}
            LightState::Unknown => {}
        }
    }
    packets
}

#[cfg(test)]
mod tests {

    use super::*;
    use std::collections::BTreeMap;
    use testing_helper::debug_packet::{loop_over_all_variants, DevicesVec};
    use testing_helper::serializers::serialize_publish;
    use testing_helper::snapshotable_types::LatestLightStateSnapshotSerlize1LineMapString;

    #[derive(PartialEq, Debug, Clone, serde::Serialize)]
    enum ModeSnapshotable {
        Brightness,
        Colour,
        Warm,
    }

    #[derive(serde::Serialize)]
    struct GlobalStateSnapshot {
        lights_brightness: u8,
        lights_warmth: f32,
        lights_hue: u16,
        last_user_input: u64,
        current_mode: ModeSnapshotable,
        last_lights_state: BTreeMap<&'static str, LatestLightStateSnapshotSerlize1LineMapString>,
    }
    impl From<DerivedState> for GlobalStateSnapshot {
        fn from(val: DerivedState) -> Self {
            GlobalStateSnapshot {
                lights_brightness: val.lights_brightness,
                lights_warmth: val.lights_warmth,
                lights_hue: val.lights_hue,
                last_user_input: val.last_user_input.elapsed().as_secs(),
                current_mode: match val.current_mode {
                    Mode::Brightness => ModeSnapshotable::Brightness,
                    Mode::Colour => ModeSnapshotable::Colour,
                    Mode::Warm => ModeSnapshotable::Warm,
                },
                last_lights_state: val
                    .last_lights_state
                    .iter()
                    .map(|(key, val)| (key.to_owned(), val.into()))
                    .collect(),
            }
        }
    }

    #[derive(serde::Serialize)]
    struct Info {
        original_state: GlobalStateSnapshot,
        #[serde(serialize_with = "serialize_publish")]
        mqtt_packet_received: Publish,
    }

    async fn get_packets(
        packet_to_send: Publish,
        arc_global_state: Arc<Mutex<DerivedState>>,
    ) -> Vec<Publish> {
        let state_changed = handle_publish(packet_to_send, arc_global_state.clone()).await;
        if state_changed {
            return publish_new_light_state(arc_global_state.clone()).await;
        }
        vec![]
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn default_global() {
        // env_logger::init();
        // setup
        let mut settings = insta::Settings::clone_current();
        settings.set_description("Testing default state with various MQTT packets");
        settings.set_snapshot_path("../../snapshots/all-lights/default_global");
        settings.set_prepend_module_to_snapshot(false);

        let device_topic_vec = vec![
            (DevicesVec::Light, Some(BEDROOM_LIGHT_PORT_BACK)),
            (DevicesVec::ShellyResult, None),
            (DevicesVec::ShellyTeleSensor, None),
            (DevicesVec::ShellyStatPower, None),
            (DevicesVec::TapDial, Some(PHILIPS_HUE_DIAL)),
        ];

        loop_over_all_variants::<_, GlobalStateSnapshot, _, _, _>(
            DerivedState::default,
            device_topic_vec,
            |publish, global_state| Info {
                original_state: global_state.into(),
                mqtt_packet_received: publish.clone(),
            },
            settings,
            |packet_to_send, arc_global_state| get_packets(packet_to_send, arc_global_state),
        )
        .await;
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn brightness_255() {
        // setup
        let mut settings = insta::Settings::clone_current();
        settings.set_description("Testing default state with various MQTT packets");
        settings.set_snapshot_path("../../snapshots/all-lights/brightness_255");
        settings.set_prepend_module_to_snapshot(false);

        let device_topic_vec = vec![
            (DevicesVec::Light, Some(BEDROOM_LIGHT_PORT_BACK)),
            (DevicesVec::ShellyResult, None),
            (DevicesVec::ShellyTeleSensor, None),
            (DevicesVec::ShellyStatPower, None),
            (DevicesVec::TapDial, Some(PHILIPS_HUE_DIAL)),
        ];

        loop_over_all_variants::<_, GlobalStateSnapshot, _, _, _>(
            || DerivedState {
                lights_brightness: 255,
                ..DerivedState::default()
            },
            device_topic_vec,
            |publish, global_state| Info {
                original_state: global_state.into(),
                mqtt_packet_received: publish.clone(),
            },
            settings,
            |packet_to_send, arc_global_state| get_packets(packet_to_send, arc_global_state),
        )
        .await;
    }
}
