use log::trace;
use rumqttc::Event;
use rumqttc::{Incoming, MqttOptions, Publish, QoS};
use std::collections::HashMap;
use std::sync::Arc;
use std::time::Instant;
use tokio::task;
use tokio::time::Duration;

use skipper::global_state::latest_lights_state::{
    HasLightsStateHashMap, LatestLightState, NoEntryInHashMap,
};
use skipper::global_state::latest_shelly_toggle_packet::{
    HasLatestShellyToggle, LatestShellyToggle,
};
use skipper::global_state::shelly_power_state::{
    HasLatestShellyPowerState, LatestShellyPowerState, ShellyPowerState,
};
use skipper::global_state::shelly_rule_timer_instant::{
    HasShellyRuleTimerInstant, ShellyRuleTimerInstant,
};
use skipper::light_controls::{
    handle_light_publish_and_update_overall_light_state, handle_light_switch_toggle_publish,
    HasOverallLightState, LightState,
};
// use crate::common::mockable_client::MockableClient;
use rumqttc::AsyncClient;
use serde::{Deserialize, Serialize};
use skipper::mqtt_topics::*;
use skipper::send_packets::publish_all_packets;
use skipper::shelly_events::{handle_shelly_state_change, maintain_shelly_state};
use tokio::sync::Mutex;

#[derive(Debug, Clone)]
struct DinetteGlobalState {
    overall_lights_state: LightState,
    last_shelly_toggle_instant: LatestShellyToggle,
    last_shelly_rule_timer_sent: ShellyRuleTimerInstant,
    last_shelly_power2_state: LatestShellyPowerState,
    last_lights_state: HashMap<&'static str, LatestLightState>,
}

impl Default for DinetteGlobalState {
    fn default() -> Self {
        DinetteGlobalState {
            overall_lights_state: LightState::Unknown,
            last_shelly_toggle_instant: Instant::now(),
            last_shelly_rule_timer_sent: Instant::now(),
            last_shelly_power2_state: LatestShellyPowerState {
                last_contact: Instant::now(),
                power_state: ShellyPowerState::On,
            },
            last_lights_state: HashMap::from([
                (DINETTE_LIGHT_PORT_BACK, LatestLightState::default()),
                (DINETTE_LIGHT_PORT_FRONT, LatestLightState::default()),
                (DINETTE_LIGHT_STARBOARD_BACK, LatestLightState::default()),
                (DINETTE_LIGHT_STARBOARD_FRONT, LatestLightState::default()),
            ]),
        }
    }
}

impl HasLatestShellyPowerState for DinetteGlobalState {
    fn clone_shelly_power_state(&self) -> LatestShellyPowerState {
        self.last_shelly_power2_state.clone()
    }

    fn update_shelly_power_state(&mut self, new_state: LatestShellyPowerState) {
        self.last_shelly_power2_state = new_state
    }
}

impl HasLightsStateHashMap for DinetteGlobalState {
    fn clone_latest_light_state(&self, light: &str) -> Option<LatestLightState> {
        self.last_lights_state.get(light).cloned()
    }

    fn update_latest_light_state(
        &mut self,
        light: &str,
        new_state: LatestLightState,
    ) -> Result<(), NoEntryInHashMap> {
        let light_state = self.last_lights_state.get_mut(light);
        match light_state {
            None => Err(NoEntryInHashMap {
                attempted_key: light.to_string(),
            }),
            Some(current_state) => {
                *current_state = new_state;
                Ok(())
            }
        }
    }
}

impl HasOverallLightState for DinetteGlobalState {
    fn clone_light_state(&self) -> LightState {
        self.overall_lights_state.clone()
    }

    fn update_light_state(&mut self, new_state: LightState) {
        self.overall_lights_state = new_state;
    }
}

impl HasLatestShellyToggle for DinetteGlobalState {
    fn clone_shelly_toggle_state(&self) -> LatestShellyToggle {
        self.last_shelly_toggle_instant
    }

    fn update_shelly_toggle_state(&mut self, new_state: LatestShellyToggle) {
        self.last_shelly_toggle_instant = new_state;
    }
}

impl HasShellyRuleTimerInstant for DinetteGlobalState {
    fn clone_shelly_rule_timer_instant(&self) -> ShellyRuleTimerInstant {
        self.last_shelly_rule_timer_sent
    }

    fn update_shelly_rule_timer_instant(&mut self, new_state: ShellyRuleTimerInstant) {
        self.last_shelly_rule_timer_sent = new_state;
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct Action {
    #[serde(rename = "Action")]
    action: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct ShellyResult {
    #[serde(rename = "Switch1")]
    switch1: Action,
}

#[tokio::main]
async fn main() {
    env_logger::init();
    let mut mqttoptions = MqttOptions::new("coordinator-dinette-down-lights", "100.96.69.67", 1883);
    mqttoptions.set_keep_alive(Duration::from_secs(5));

    let (client, mut eventloop) = AsyncClient::new(mqttoptions, 10);
    skipper::mqtt_helpers::confirm_mqtt_connection(
        "coordinator-dinette-down-lights",
        &mut eventloop,
    )
    .await
    .expect("Connected to broker");

    let subscription_topics = [
        DINETTE_LIGHT_PORT_BACK.to_owned(),
        DINETTE_LIGHT_PORT_FRONT.to_owned(),
        DINETTE_LIGHT_STARBOARD_FRONT.to_owned(),
        DINETTE_LIGHT_STARBOARD_BACK.to_owned(),
        SHELLY3_RESULT.to_owned(),
        SHELLY3.to_owned() + "/" + SHELLY_CMND_POWER,
        SHELLY3.to_owned() + "/" + SHELLY_CMND_POWER2,
        SHELLY3.to_owned() + "/" + SHELLY_TELE_SENSOR,
    ];
    for topic in &subscription_topics {
        client.subscribe(topic, QoS::AtMostOnce).await.unwrap();
    }

    trace!("Subscribed to {:?}", subscription_topics);

    let derived_state = Arc::new(Mutex::new(DinetteGlobalState::default()));
    match client
        .publish(
            SHELLY3.to_owned() + "/" + SHELLY_CMND_POWER2,
            QoS::AtLeastOnce,
            false,
            *b"ON",
        )
        .await
    {
        Ok(_) => {}
        Err(_) => todo!(),
    }
    while let Ok(notification) = eventloop.poll().await {
        match notification {
            Event::Incoming(packet) => match packet {
                Incoming::Connect(_) => {}
                Incoming::ConnAck(_) => {}
                Incoming::Publish(publish) => {
                    let cloned_client: AsyncClient = client.clone();
                    let cloned_ephemeral_state = derived_state.clone();
                    let publish = dbg!(publish);
                    task::spawn(async move {
                        let packets = handle_publish(publish, cloned_ephemeral_state).await;
                        publish_all_packets(packets, cloned_client).await;
                    });
                }
                Incoming::PubAck(_) => {}
                Incoming::PubRec(_) => {}
                Incoming::PubRel(_) => {}
                Incoming::PubComp(_) => {}
                Incoming::Subscribe(_) => {}
                Incoming::SubAck(_) => {}
                Incoming::Unsubscribe(_) => {}
                Incoming::UnsubAck(_) => {}
                Incoming::PingReq => {}
                Incoming::PingResp => {}
                Incoming::Disconnect => {}
            },
            Event::Outgoing(_packet) => {}
        }
    }
}

async fn handle_publish(
    publish: Publish,
    derived_state: Arc<Mutex<DinetteGlobalState>>,
) -> Vec<rumqttc::Publish> {
    let mut packets = vec![];
    trace!(
        "packet received: {:?}, packet payload: {:?}, current_state: {:?}",
        publish,
        std::str::from_utf8(&publish.payload).expect("MQTT payload utf8 encoded"),
        &derived_state.lock().await
    );
    if publish.topic == SHELLY3_RESULT {
        packets.append(
            &mut handle_light_switch_toggle_publish(
                publish,
                derived_state,
                &[
                    DINETTE_LIGHT_PORT_BACK,
                    DINETTE_LIGHT_PORT_FRONT,
                    DINETTE_LIGHT_STARBOARD_FRONT,
                    DINETTE_LIGHT_STARBOARD_BACK,
                ],
            )
            .await,
        );
    } else if publish.topic == DINETTE_LIGHT_PORT_BACK
        || publish.topic == DINETTE_LIGHT_PORT_FRONT
        || publish.topic == DINETTE_LIGHT_STARBOARD_FRONT
        || publish.topic == DINETTE_LIGHT_STARBOARD_BACK
    {
        packets.append(
            &mut handle_light_publish_and_update_overall_light_state(
                publish,
                derived_state,
                SHELLY3,
            )
            .await,
        );
    } else if publish.topic == format!("{}/{}", SHELLY3, SHELLY_STAT_POWER2) {
        packets.append(
            &mut handle_shelly_state_change(
                publish,
                derived_state,
                SHELLY3,
                SHELLY_CMND_POWER2,
                &[
                    DINETTE_LIGHT_PORT_BACK,
                    DINETTE_LIGHT_PORT_FRONT,
                    DINETTE_LIGHT_STARBOARD_FRONT,
                    DINETTE_LIGHT_STARBOARD_BACK,
                ],
            )
            .await,
        );
    } else if publish.topic == format!("{}/{}", SHELLY3, SHELLY_TELE_SENSOR) {
        packets.append(
            &mut maintain_shelly_state(
                publish,
                derived_state,
                SHELLY3,
                SHELLY_CMND_POWER2,
                &[
                    DINETTE_LIGHT_PORT_BACK,
                    DINETTE_LIGHT_PORT_FRONT,
                    DINETTE_LIGHT_STARBOARD_FRONT,
                    DINETTE_LIGHT_STARBOARD_BACK,
                ],
            )
            .await,
        );
    }
    packets
}

#[cfg(test)]
mod tests {
    use super::*;
    use skipper::global_state::shelly_power_state::ShellyPowerState;
    use std::collections::BTreeMap;
    use testing_helper::debug_packet::{loop_over_all_variants, DevicesVec};
    use testing_helper::serializers::serialize_publish;
    use testing_helper::snapshotable_types::LatestLightStateSnapshotSerlize1LineMapString;

    #[derive(serde::Serialize)]
    struct LatestShellyPowerStateSnapshot {
        pub last_contact: u64,
        pub power_state: ShellyPowerState,
    }

    #[derive(serde::Serialize)]
    struct GlobalStateSnapshot {
        overall_lights_state: LightState,
        last_shelly_toggle_instant: u64,
        last_shelly_rule_timer_sent: u64,
        last_shelly_power1_state: LatestShellyPowerStateSnapshot,
        last_lights_state: BTreeMap<&'static str, LatestLightStateSnapshotSerlize1LineMapString>,
    }
    impl From<DinetteGlobalState> for GlobalStateSnapshot {
        fn from(val: DinetteGlobalState) -> Self {
            GlobalStateSnapshot {
                overall_lights_state: val.overall_lights_state,
                last_shelly_toggle_instant: val.last_shelly_toggle_instant.elapsed().as_secs(),
                last_shelly_rule_timer_sent: val.last_shelly_rule_timer_sent.elapsed().as_secs(),
                last_shelly_power1_state: LatestShellyPowerStateSnapshot {
                    last_contact: val
                        .last_shelly_power2_state
                        .last_contact
                        .elapsed()
                        .as_secs(),
                    power_state: val.last_shelly_power2_state.power_state,
                },
                last_lights_state: val
                    .last_lights_state
                    .iter()
                    .map(|(key, val)| (key.to_owned(), val.into()))
                    .collect(),
            }
        }
    }

    #[derive(serde::Serialize)]
    struct Info {
        original_state: GlobalStateSnapshot,
        #[serde(serialize_with = "serialize_publish")]
        mqtt_packet_received: Publish,
    }

    #[derive(serde::Serialize)]
    pub struct InfoOrdered<'a> {
        info: &'a Info,
        order_num: i32,
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn default_global() {
        // setup
        let mut settings = insta::Settings::clone_current();
        settings.set_description("Testing default state with various MQTT packets");
        settings.set_snapshot_path("../../snapshots/dinette-lights/default_global");
        settings.set_prepend_module_to_snapshot(false);

        let device_topic_vec = vec![
            (DevicesVec::Light, Some(DINETTE_LIGHT_PORT_BACK)),
            (DevicesVec::ShellyResult, Some(SHELLY3_RESULT)),
            (DevicesVec::ShellyTeleSensor, Some(SHELLY3_TELE_SENSOR)),
            (DevicesVec::ShellyStatPower, Some(SHELLY3_STAT_POWER2)),
            (DevicesVec::TapDial, None),
        ];

        loop_over_all_variants::<_, GlobalStateSnapshot, _, _, _>(
            DinetteGlobalState::default,
            device_topic_vec,
            |publish, global_state| Info {
                original_state: global_state.into(),
                mqtt_packet_received: publish.clone(),
            },
            settings,
            |packet_to_send, arc_global_state| handle_publish(packet_to_send, arc_global_state),
        )
        .await;
    }
}
