use serde::{Deserialize, Serialize};
use std::time;

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum ShellyPowerState {
    #[serde(rename = "ON")]
    On,
    #[serde(rename = "OFF")]
    Off,
}

#[derive(Debug, Clone)]
pub struct LatestShellyPowerState {
    pub last_contact: time::Instant,
    pub power_state: ShellyPowerState,
}

pub trait HasLatestShellyPowerState {
    fn clone_shelly_power_state(&self) -> LatestShellyPowerState;
    fn update_shelly_power_state(&mut self, new_state: LatestShellyPowerState);
}
