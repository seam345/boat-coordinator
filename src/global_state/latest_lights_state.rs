use crate::light_controls::LightState;
use std::fmt;
use std::time::Instant;

// Define our error types. These may be customized for our error handling cases.
// Now we will be able to write our own errors, defer to an underlying error
// implementation, or do something in between.
#[derive(Debug, Clone)]
pub struct NoEntryInHashMap {
    pub attempted_key: String,
}

// Generation of an error is completely separate from how it is displayed.
// There's no need to be concerned about cluttering complex logic with the display style.
//
// Note that we don't store any extra info about the errors. This means we can't state
// which string failed to parse without modifying our types to carry that information.
impl fmt::Display for NoEntryInHashMap {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} Doesn't exist in HashMap", self.attempted_key)
    }
}

#[derive(Debug, Clone)]
pub struct HsColour {
    pub hue: u16,       // 0-360
    pub saturation: u8, // 0-100
}

#[derive(Debug, Clone)]
pub struct LatestLightState {
    pub last_state_change: std::time::Instant,
    pub last_heart_beat: std::time::Instant,
    pub state: LightState,
    pub brightness: u8,
    pub color_temp: f32, // bound between 153-500 todo consider converting to type
    pub color_hsb: HsColour,
}

impl Default for LatestLightState {
    fn default() -> Self {
        LatestLightState {
            last_state_change: Instant::now(),
            last_heart_beat: Instant::now(),
            state: LightState::Unknown,
            brightness: 128,
            color_temp: 153.0,
            color_hsb: HsColour {
                hue: 0,
                saturation: 100,
            },
        }
    }
}

// type LatestLightsState = HashMap<&'static str, LatestLightState>;

// hashmap not trivial to clone so we are just going to allow the cloning of a single LatestLightState
pub trait HasLightsStateHashMap {
    fn clone_latest_light_state(&self, light: &str) -> Option<LatestLightState>;
    fn update_latest_light_state(
        &mut self,
        light: &str,
        new_state: LatestLightState,
    ) -> Result<(), NoEntryInHashMap>;
}
