pub type ShellyRuleTimerInstant = std::time::Instant;

pub trait HasShellyRuleTimerInstant {
    fn clone_shelly_rule_timer_instant(&self) -> ShellyRuleTimerInstant;
    fn update_shelly_rule_timer_instant(&mut self, new_state: ShellyRuleTimerInstant);
}
