pub type LatestShellyToggle = std::time::Instant;

pub trait HasLatestShellyToggle {
    fn clone_shelly_toggle_state(&self) -> LatestShellyToggle;
    fn update_shelly_toggle_state(&mut self, new_state: LatestShellyToggle);
}
