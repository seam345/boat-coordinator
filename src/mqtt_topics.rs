pub const SHELLY1: &str = "shelly/sh1"; // Bedroom light switch, power1, switch 1
pub const SHELLY2: &str = "shelly/sh2"; // front room light switch, power1, switch 1
pub const SHELLY3: &str = "shelly/sh3"; // dinette light switch, power2
pub const SHELLY4: &str = "shelly/sh4"; // bathroom light switch
pub const SHELLY5: &str = "shelly/sh5"; // engine room light switch
pub const SHELLY6: &str = "shelly/sh6"; // kitchen light switch
pub const SHELLY_STAT_RESULT: &str = "stat/RESULT";
pub const SHELLY_CMND_POWER: &str = "cmnd/Power";
pub const SHELLY_CMND_POWER1: &str = "cmnd/POWER1";
pub const SHELLY_CMND_POWER2: &str = "cmnd/POWER2";
pub const SHELLY_STAT_POWER1: &str = "stat/POWER1";
pub const SHELLY_STAT_POWER2: &str = "stat/POWER2";
pub const SHELLY_TELE_SENSOR: &str = "tele/SENSOR";
pub const SHELLY1_RESULT: &str = "shelly/sh1/stat/RESULT";
pub const SHELLY2_RESULT: &str = "shelly/sh2/stat/RESULT";
pub const SHELLY3_RESULT: &str = "shelly/sh3/stat/RESULT";
pub const SHELLY4_RESULT: &str = "shelly/sh4/stat/RESULT";
pub const SHELLY5_RESULT: &str = "shelly/sh5/stat/RESULT";
pub const SHELLY6_RESULT: &str = "shelly/sh6/stat/RESULT";
pub const SHELLY1_STAT_POWER1: &str = "shelly/sh1/stat/POWER1";
pub const SHELLY2_STAT_POWER1: &str = "shelly/sh2/stat/POWER1";
pub const SHELLY3_STAT_POWER1: &str = "shelly/sh3/stat/POWER1";
pub const SHELLY4_STAT_POWER1: &str = "shelly/sh4/stat/POWER1";
pub const SHELLY5_STAT_POWER1: &str = "shelly/sh5/stat/POWER1";
pub const SHELLY6_STAT_POWER1: &str = "shelly/sh6/stat/POWER1";
pub const SHELLY1_STAT_POWER2: &str = "shelly/sh1/stat/POWER2";
pub const SHELLY2_STAT_POWER2: &str = "shelly/sh2/stat/POWER2";
pub const SHELLY3_STAT_POWER2: &str = "shelly/sh3/stat/POWER2";
pub const SHELLY4_STAT_POWER2: &str = "shelly/sh4/stat/POWER2";
pub const SHELLY5_STAT_POWER2: &str = "shelly/sh5/stat/POWER2";
pub const SHELLY6_STAT_POWER2: &str = "shelly/sh6/stat/POWER2";
pub const SHELLY1_TELE_SENSOR: &str = "shelly/sh1/tele/SENSOR";
pub const SHELLY2_TELE_SENSOR: &str = "shelly/sh2/tele/SENSOR";
pub const SHELLY3_TELE_SENSOR: &str = "shelly/sh3/tele/SENSOR";
pub const SHELLY4_TELE_SENSOR: &str = "shelly/sh4/tele/SENSOR";
pub const SHELLY5_TELE_SENSOR: &str = "shelly/sh5/tele/SENSOR";
pub const SHELLY6_TELE_SENSOR: &str = "shelly/sh6/tele/SENSOR";

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn const_strings_equal() {
        assert_eq!(
            SHELLY1_RESULT,
            format!("{}/{}", SHELLY1, SHELLY_STAT_RESULT)
        );
        assert_eq!(
            SHELLY2_RESULT,
            format!("{}/{}", SHELLY2, SHELLY_STAT_RESULT)
        );
        assert_eq!(
            SHELLY3_RESULT,
            format!("{}/{}", SHELLY3, SHELLY_STAT_RESULT)
        );
        assert_eq!(
            SHELLY4_RESULT,
            format!("{}/{}", SHELLY4, SHELLY_STAT_RESULT)
        );
        assert_eq!(
            SHELLY5_RESULT,
            format!("{}/{}", SHELLY5, SHELLY_STAT_RESULT)
        );
        assert_eq!(
            SHELLY6_RESULT,
            format!("{}/{}", SHELLY6, SHELLY_STAT_RESULT)
        );
        assert_eq!(
            SHELLY1_TELE_SENSOR,
            format!("{}/{}", SHELLY1, SHELLY_TELE_SENSOR)
        );
        assert_eq!(
            SHELLY2_TELE_SENSOR,
            format!("{}/{}", SHELLY2, SHELLY_TELE_SENSOR)
        );
        assert_eq!(
            SHELLY3_TELE_SENSOR,
            format!("{}/{}", SHELLY3, SHELLY_TELE_SENSOR)
        );
        assert_eq!(
            SHELLY4_TELE_SENSOR,
            format!("{}/{}", SHELLY4, SHELLY_TELE_SENSOR)
        );
        assert_eq!(
            SHELLY5_TELE_SENSOR,
            format!("{}/{}", SHELLY5, SHELLY_TELE_SENSOR)
        );
        assert_eq!(
            SHELLY6_TELE_SENSOR,
            format!("{}/{}", SHELLY6, SHELLY_TELE_SENSOR)
        );
        assert_eq!(
            SHELLY1_STAT_POWER1,
            format!("{}/{}", SHELLY1, SHELLY_STAT_POWER1)
        );
        assert_eq!(
            SHELLY2_STAT_POWER1,
            format!("{}/{}", SHELLY2, SHELLY_STAT_POWER1)
        );
        assert_eq!(
            SHELLY3_STAT_POWER1,
            format!("{}/{}", SHELLY3, SHELLY_STAT_POWER1)
        );
        assert_eq!(
            SHELLY4_STAT_POWER1,
            format!("{}/{}", SHELLY4, SHELLY_STAT_POWER1)
        );
        assert_eq!(
            SHELLY5_STAT_POWER1,
            format!("{}/{}", SHELLY5, SHELLY_STAT_POWER1)
        );
        assert_eq!(
            SHELLY6_STAT_POWER1,
            format!("{}/{}", SHELLY6, SHELLY_STAT_POWER1)
        );
        assert_eq!(
            SHELLY1_STAT_POWER2,
            format!("{}/{}", SHELLY1, SHELLY_STAT_POWER2)
        );
        assert_eq!(
            SHELLY2_STAT_POWER2,
            format!("{}/{}", SHELLY2, SHELLY_STAT_POWER2)
        );
        assert_eq!(
            SHELLY3_STAT_POWER2,
            format!("{}/{}", SHELLY3, SHELLY_STAT_POWER2)
        );
        assert_eq!(
            SHELLY4_STAT_POWER2,
            format!("{}/{}", SHELLY4, SHELLY_STAT_POWER2)
        );
        assert_eq!(
            SHELLY5_STAT_POWER2,
            format!("{}/{}", SHELLY5, SHELLY_STAT_POWER2)
        );
        assert_eq!(
            SHELLY6_STAT_POWER2,
            format!("{}/{}", SHELLY6, SHELLY_STAT_POWER2)
        );
    }
}

pub const FRONTROOM_LIGHT_PORT_BACK: &str = "zigbee2mqtt/light/frontroom/port/back/spot";
pub const FRONTROOM_LIGHT_PORT_MIDDLE: &str = "zigbee2mqtt/light/frontroom/port/middle/spot";
pub const FRONTROOM_LIGHT_PORT_FRONT: &str = "zigbee2mqtt/light/frontroom/port/front/spot";
pub const FRONTROOM_LIGHT_STARBOARD_FRONT: &str =
    "zigbee2mqtt/light/frontroom/starboard/front/spot";
pub const FRONTROOM_LIGHT_STARBOARD_MIDDLE: &str =
    "zigbee2mqtt/light/frontroom/starboard/middle/spot";
pub const FRONTROOM_LIGHT_STARBOARD_BACK: &str = "zigbee2mqtt/light/frontroom/starboard/back/spot";
pub const FRONTROOM_LIGHT_GROUP: &str = "zigbee2mqtt/frontroom_lights";
pub const FRONTROOM_LIGHT_GROUP_ARRAY: &[&str] = &[
    FRONTROOM_LIGHT_PORT_BACK,
    FRONTROOM_LIGHT_PORT_MIDDLE,
    FRONTROOM_LIGHT_PORT_BACK,
    FRONTROOM_LIGHT_STARBOARD_BACK,
    FRONTROOM_LIGHT_STARBOARD_MIDDLE,
    FRONTROOM_LIGHT_STARBOARD_FRONT,
];

pub const DINETTE_LIGHT_PORT_BACK: &str = "zigbee2mqtt/light/dinette/port/back/spot";
pub const DINETTE_LIGHT_PORT_FRONT: &str = "zigbee2mqtt/light/dinette/port/front/spot";
pub const DINETTE_LIGHT_STARBOARD_FRONT: &str = "zigbee2mqtt/light/dinette/starboard/front/spot";
pub const DINETTE_LIGHT_STARBOARD_BACK: &str = "zigbee2mqtt/light/dinette/starboard/back/spot";
pub const DINETTE_LIGHT_GROUP: &str = "zigbee2mqtt/dinette_lights";
pub const DINETTE_LIGHT_GROUP_ARRAY: &[&str] = &[
    DINETTE_LIGHT_PORT_BACK,
    DINETTE_LIGHT_PORT_FRONT,
    DINETTE_LIGHT_STARBOARD_FRONT,
    DINETTE_LIGHT_STARBOARD_BACK,
];

pub const KITCHEN_LIGHT_PORT_BACK: &str = "zigbee2mqtt/light/kitchen/port/back/spot";
pub const KITCHEN_LIGHT_PORT_MIDDLE: &str = "zigbee2mqtt/light/kitchen/port/middle/spot";
pub const KITCHEN_LIGHT_PORT_FRONT: &str = "zigbee2mqtt/light/kitchen/port/front/spot";
pub const KITCHEN_LIGHT_STARBOARD_BACK: &str = "zigbee2mqtt/light/kitchen/starboard/back/spot";
pub const KITCHEN_LIGHT_STARBOARD: &str = "zigbee2mqtt/light/starboard_kitchen";
pub const KITCHEN_LIGHT_GROUP: &str = "zigbee2mqtt/kitchen_lights";
pub const KITCHEN_LIGHT_GROUP_ARRAY: &[&str] = &[
    KITCHEN_LIGHT_PORT_BACK,
    KITCHEN_LIGHT_PORT_MIDDLE,
    KITCHEN_LIGHT_PORT_FRONT,
    KITCHEN_LIGHT_STARBOARD_BACK,
    KITCHEN_LIGHT_STARBOARD,
];

pub const BATHROOM_LIGHT_PORT_BACK: &str = "zigbee2mqtt/light/bathroom/port/back/spot";
pub const BATHROOM_LIGHT_PORT_FRONT: &str = "zigbee2mqtt/light/bathroom/port/front/spot";
pub const BATHROOM_LIGHT_STARBOARD_FRONT: &str = "zigbee2mqtt/light/bathroom/starboard/front/spot";
pub const BATHROOM_LIGHT_STARBOARD_MIDDLE: &str =
    "zigbee2mqtt/light/bathroom/starboard/middle/spot";
pub const BATHROOM_LIGHT_STARBOARD_BACK: &str = "zigbee2mqtt/light/bathroom/starboard/back/spot";
pub const BATHROOM_LIGHT_GROUP: &str = "zigbee2mqtt/bathroom_lights";
pub const BATHROOM_LIGHT_GROUP_ARRAY: &[&str] = &[
    BATHROOM_LIGHT_PORT_BACK,
    BATHROOM_LIGHT_PORT_FRONT,
    BATHROOM_LIGHT_STARBOARD_BACK,
    BATHROOM_LIGHT_STARBOARD_MIDDLE,
    BATHROOM_LIGHT_STARBOARD_FRONT,
];

pub const BEDROOM_LIGHT_PORT_BACK: &str = "zigbee2mqtt/light/spot/bedroom_port_back";
pub const BEDROOM_LIGHT_PORT_FRONT: &str = "zigbee2mqtt/light/spot/bedroom_port_front";
pub const BEDROOM_LIGHT_STARBOARD_FRONT: &str = "zigbee2mqtt/light/spot/bedroom_starboard_front";
pub const BEDROOM_LIGHT_STARBOARD_BACK: &str = "zigbee2mqtt/light/spot/bedroom_starboard_back";
pub const BEDROOM_LIGHT_STARBOARD_MIDDLE: &str = "zigbee2mqtt/light/spot/bedroom_starboard_middle";
pub const BEDROOM_LIGHT_GROUP: &str = "zigbee2mqtt/bedroom_lights";
pub const BEDROOM_LIGHT_GROUP_ARRAY: &[&str] = &[
    BEDROOM_LIGHT_PORT_BACK,
    BEDROOM_LIGHT_PORT_FRONT,
    BEDROOM_LIGHT_STARBOARD_BACK,
    BEDROOM_LIGHT_STARBOARD_MIDDLE,
    BEDROOM_LIGHT_STARBOARD_FRONT,
];

pub const ENGINEROOM_LIGHT_PORT_BACK: &str = "zigbee2mqtt/light/engineroom/port/spot";
pub const ENGINEROOM_LIGHT_STARBOARD_FRONT: &str = "zigbee2mqtt/light/engineroom/starboard/spot";

pub const HABITABLE_LIGHT_GROUP: &str = "zigbee2mqtt/habitable_area_lights";
pub const HABITABLE_LIGHT_GROUP_ARRAY: &[&str] = &[
    FRONTROOM_LIGHT_PORT_BACK,
    FRONTROOM_LIGHT_PORT_MIDDLE,
    FRONTROOM_LIGHT_PORT_BACK,
    FRONTROOM_LIGHT_STARBOARD_BACK,
    FRONTROOM_LIGHT_STARBOARD_MIDDLE,
    FRONTROOM_LIGHT_STARBOARD_FRONT,
    DINETTE_LIGHT_PORT_BACK,
    DINETTE_LIGHT_PORT_FRONT,
    DINETTE_LIGHT_STARBOARD_FRONT,
    DINETTE_LIGHT_STARBOARD_BACK,
    KITCHEN_LIGHT_PORT_BACK,
    KITCHEN_LIGHT_PORT_MIDDLE,
    KITCHEN_LIGHT_PORT_FRONT,
    KITCHEN_LIGHT_STARBOARD_BACK,
    KITCHEN_LIGHT_STARBOARD,
    BATHROOM_LIGHT_PORT_BACK,
    BATHROOM_LIGHT_PORT_FRONT,
    BATHROOM_LIGHT_STARBOARD_BACK,
    BATHROOM_LIGHT_STARBOARD_MIDDLE,
    BATHROOM_LIGHT_STARBOARD_FRONT,
    BEDROOM_LIGHT_PORT_BACK,
    BEDROOM_LIGHT_PORT_FRONT,
    BEDROOM_LIGHT_STARBOARD_BACK,
    BEDROOM_LIGHT_STARBOARD_MIDDLE,
    BEDROOM_LIGHT_STARBOARD_FRONT,
];

pub const LIVING_LIGHT_GROUP: &str = "zigbee2mqtt/living_area_lights";
pub const LIVING_LIGHT_GROUP_ARRAY: &[&str] = &[
    FRONTROOM_LIGHT_PORT_BACK,
    FRONTROOM_LIGHT_PORT_MIDDLE,
    FRONTROOM_LIGHT_PORT_BACK,
    FRONTROOM_LIGHT_STARBOARD_BACK,
    FRONTROOM_LIGHT_STARBOARD_MIDDLE,
    FRONTROOM_LIGHT_STARBOARD_FRONT,
    DINETTE_LIGHT_PORT_BACK,
    DINETTE_LIGHT_PORT_FRONT,
    DINETTE_LIGHT_STARBOARD_FRONT,
    DINETTE_LIGHT_STARBOARD_BACK,
    KITCHEN_LIGHT_PORT_BACK,
    KITCHEN_LIGHT_PORT_MIDDLE,
    KITCHEN_LIGHT_PORT_FRONT,
    KITCHEN_LIGHT_STARBOARD_BACK,
    KITCHEN_LIGHT_STARBOARD,
];
