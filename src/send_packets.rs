use log;
use rumqttc::AsyncClient;

pub async fn publish_all_packets(packets: Vec<rumqttc::Publish>, mqtt_client: AsyncClient) {
    for packet in packets {
        log::trace!("Sending packet sent {:?}", packet);
        match mqtt_client
            .publish(packet.topic, packet.qos, packet.retain, packet.payload)
            .await
        {
            Ok(_) => {
                log::trace!("Packet successfully sent")
            }
            Err(_) => todo!(),
        };
    }
}
