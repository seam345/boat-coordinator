pub mod global_state {
    pub mod latest_lights_state;
    pub mod latest_shelly_toggle_packet;
    pub mod shelly_power_state;
    pub mod shelly_rule_timer_instant;
}
pub mod light_controls;
pub mod mqtt_helpers;
pub mod mqtt_topics;
pub mod send_packets;
pub mod shelly_events;
pub mod zigbee_light;
