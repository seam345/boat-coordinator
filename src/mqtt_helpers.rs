use log::{debug, error, trace};
use rumqttc::{ConnectionError, EventLoop};
use std::time::Duration;

/// Confirms connection to MQTT broker, retries for 10 min then Errors out
///
/// # Arguments
///
/// * `client_id`: static string of the name of the client connecting to mqtt
/// * `eventloop`: rumqttc::EventLoop after calling MqttOptions::new()
///
/// returns: ()
///
/// # Examples
///
/// ```ignore
/// use rumqttc::{AsyncClient, MqttOptions};
/// let mut mqttoptions = MqttOptions::new("MQTT_CLIENT_ID", "<host address>", 1883);
///
/// let (client, mut eventloop) = AsyncClient::new(mqttoptions, 10);
/// skipper::mqtt_helpers::confirm_mqtt_connection(MQTT_CLIENT_ID, &mut eventloop).await.expect("panic message");
/// ```
pub async fn confirm_mqtt_connection(
    client_id: &'static str,
    eventloop: &mut EventLoop,
) -> Result<(), ConnectionError> {
    let connection_time = std::time::Instant::now();
    debug!("confirm connection is successful");
    while let Err(error) = eventloop.poll().await {
        trace!("event loop failing with {:?}", error);
        if std::time::Instant::now() - connection_time > Duration::new(600, 0) {
            error!("Failed to connect for 10 Min");
            return Err(error);
        }
    }
    debug!("{} connected to broker", client_id);
    Ok(())
}
