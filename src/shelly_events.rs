use crate::global_state::latest_lights_state::{HasLightsStateHashMap, LatestLightState};
use crate::global_state::latest_shelly_toggle_packet::HasLatestShellyToggle;
use crate::global_state::shelly_power_state::{
    HasLatestShellyPowerState, LatestShellyPowerState, ShellyPowerState,
};
use crate::global_state::shelly_rule_timer_instant::HasShellyRuleTimerInstant;
use crate::light_controls::{maintain_state, HasOverallLightState, LightState};
use rumqttc::Publish;
use serde::Deserialize;
use serde::Serialize;
use std::sync::Arc;
use std::time::Instant;
use tokio::sync::Mutex;

#[derive(Serialize, Deserialize, Debug)]
struct ShellyResult {
    #[serde(rename = "Switch1")]
    switch1: ShellyPowerState,

    #[serde(rename = "Switch2")]
    switch2: ShellyPowerState,
}

pub async fn handle_shelly_state_change<G>(
    publish: Publish,
    global_state: Arc<Mutex<G>>,
    shelly_mqtt_endpoint: &str,
    shelly_power_endpoint: &str,
    light_endpoints: &[&'static str],
) -> Vec<rumqttc::Publish>
where
    G: HasOverallLightState
        + HasLatestShellyToggle
        + HasShellyRuleTimerInstant
        + HasLightsStateHashMap
        + HasLatestShellyPowerState
        + Send
        + 'static,
{
    let mut packets = vec![];
    let parsed: ShellyPowerState = match std::str::from_utf8(&publish.payload).unwrap() {
        "ON" => ShellyPowerState::On,
        "OFF" => ShellyPowerState::Off,
        _ => {
            log::error!(
                "Failed to parse shelly_state_change bytes: {:?}",
                publish.payload
            );
            return packets;
        }
    };
    log::debug!("shelly power state parsed as: {:?}", &parsed);

    match parsed {
        ShellyPowerState::On => {
            global_state
                .lock()
                .await
                .update_shelly_power_state(LatestShellyPowerState {
                    last_contact: Instant::now(),
                    power_state: ShellyPowerState::On,
                });
        }
        ShellyPowerState::Off => {
            log::debug!("powered off attempting to bring back");
            // todo compute time elapsed between this event and last toggle, if it's within 3 seconds treat it as a failed toggle event and toggle lights
            // else treat it as a strange power off case and turn back on and restore state
            let last_switch_toggle = global_state.lock().await.clone_shelly_toggle_state();
            global_state
                .lock()
                .await
                .update_shelly_power_state(LatestShellyPowerState {
                    last_contact: Instant::now(),
                    power_state: ShellyPowerState::Off,
                });

            let sgs = global_state.clone();
            if dbg!(last_switch_toggle.elapsed().as_secs()) > 3 {
                //maintain state

                packets.append(
                    &mut maintain_state(
                        sgs,
                        shelly_mqtt_endpoint,
                        shelly_power_endpoint,
                        light_endpoints,
                    )
                    .await,
                );
            } else {
                let global_lock = global_state.lock().await;
                let mut light_states: Vec<LatestLightState> = Vec::new();
                for topic in light_endpoints {
                    light_states.push(global_lock.clone_latest_light_state(topic).unwrap());
                }
                drop(global_lock); // release lock

                let mut light_switch_toggled_after_last_light_update = true;
                'state_check: for light_state in light_states {
                    if light_state
                        .last_state_change
                        .checked_duration_since(last_switch_toggle)
                        .is_some()
                    {
                        light_switch_toggled_after_last_light_update = false;
                        break 'state_check;
                    }
                }

                if light_switch_toggled_after_last_light_update {
                    // state has not been updated properly turn back on and swap state
                    let mut global_lock = global_state.lock().await;
                    match global_lock.clone_light_state() {
                        LightState::On => global_lock.update_light_state(LightState::Off),
                        LightState::Off => global_lock.update_light_state(LightState::On),
                        LightState::Unknown => global_lock.update_light_state(LightState::Off),
                    }
                    drop(global_lock); // release lock
                    dbg!("change state");
                    packets.append(
                        &mut maintain_state(
                            sgs,
                            shelly_mqtt_endpoint,
                            shelly_power_endpoint,
                            light_endpoints,
                        )
                        .await,
                    );
                } else {
                    // maintain state
                    dbg!("same state");
                    packets.append(
                        &mut maintain_state(
                            sgs,
                            shelly_mqtt_endpoint,
                            shelly_power_endpoint,
                            light_endpoints,
                        )
                        .await,
                    );
                }
            }
        }
    }
    packets
}

pub async fn maintain_shelly_state<G>(
    publish: Publish,
    global_state: Arc<Mutex<G>>,
    shelly_mqtt_endpoint: &str,
    shelly_power_endpoint: &str,
    light_endpoints: &[&'static str],
) -> Vec<rumqttc::Publish>
where
    G: HasOverallLightState
        + HasLatestShellyToggle
        + HasShellyRuleTimerInstant
        + HasLightsStateHashMap
        + HasLatestShellyPowerState
        + std::marker::Send
        + 'static,
{
    let mut packets = vec![];
    let parsed: ShellyResult =
        match serde_json::from_str(std::str::from_utf8(&publish.payload).unwrap()) {
            Ok(motion_json) => motion_json,
            Err(err) => {
                log::error!("{:?}", &publish.payload);
                log::error!("{:?}", err);
                todo!()
            }
        };
    log::debug!("parsed shelly result {:?}", &parsed);

    let current_known_state = global_state.lock().await.clone_shelly_power_state();
    match parsed.switch1 {
        ShellyPowerState::On => {
            if current_known_state.power_state == ShellyPowerState::Off {
                global_state
                    .lock()
                    .await
                    .update_shelly_power_state(LatestShellyPowerState {
                        last_contact: Instant::now(),
                        power_state: ShellyPowerState::On,
                    });
            }
        }
        ShellyPowerState::Off => {
            log::debug!("powered off attempting to bring back");
            if current_known_state.power_state == ShellyPowerState::On {
                global_state
                    .lock()
                    .await
                    .update_shelly_power_state(LatestShellyPowerState {
                        last_contact: Instant::now(),
                        power_state: ShellyPowerState::Off,
                    });
            }
            // todo compute time elapsed between this event and last toggle, if it's within 3 seconds treat it as a failed toggle event and toggle lights
            // else treat it as a strange power off case and turn back on and restore state
            let last_switch_toggle = global_state.lock().await.clone_shelly_toggle_state();

            let sgs = global_state.clone();
            if dbg!(last_switch_toggle.elapsed().as_secs()) > 3 {
                //maintain state

                packets.append(
                    &mut maintain_state(
                        sgs,
                        shelly_mqtt_endpoint,
                        shelly_power_endpoint,
                        light_endpoints,
                    )
                    .await,
                );
            } else {
                let global_lock = global_state.lock().await;
                let mut light_states: Vec<LatestLightState> = Vec::new();
                for topic in light_endpoints {
                    light_states.push(global_lock.clone_latest_light_state(topic).unwrap());
                }
                drop(global_lock); // release lock

                let mut light_switch_toggled_after_last_light_update = true;
                'state_check: for light_state in light_states {
                    if light_state
                        .last_state_change
                        .checked_duration_since(last_switch_toggle)
                        .is_some()
                    {
                        light_switch_toggled_after_last_light_update = false;
                        break 'state_check;
                    }
                }

                if light_switch_toggled_after_last_light_update {
                    // state has not been updated properly turn back on and swap state
                    let mut global_lock = global_state.lock().await;
                    match global_lock.clone_light_state() {
                        LightState::On => global_lock.update_light_state(LightState::Off),
                        LightState::Off => global_lock.update_light_state(LightState::On),
                        LightState::Unknown => global_lock.update_light_state(LightState::Off),
                    }
                    drop(global_lock); // release lock
                    log::debug!("change state");
                    packets.append(
                        &mut maintain_state(
                            sgs,
                            shelly_mqtt_endpoint,
                            shelly_power_endpoint,
                            light_endpoints,
                        )
                        .await,
                    );
                } else {
                    // maintain state
                    log::debug!("same state");
                    packets.append(
                        &mut maintain_state(
                            sgs,
                            shelly_mqtt_endpoint,
                            shelly_power_endpoint,
                            light_endpoints,
                        )
                        .await,
                    );
                }
            }
        }
    }
    packets
}
