use crate::global_state::latest_lights_state::{
    HasLightsStateHashMap, HsColour, LatestLightState, NoEntryInHashMap,
};
use crate::global_state::latest_shelly_toggle_packet::HasLatestShellyToggle;
use crate::global_state::shelly_rule_timer_instant::HasShellyRuleTimerInstant;
use crate::zigbee_light::{LightResult, ZigbeeState};
use log::{error, info, trace};
use rumqttc::{Publish, QoS};
use serde::{Deserialize, Serialize};
use std::str::from_utf8;
use std::sync::Arc;
use std::time::Instant;
use tokio::sync::Mutex;
// use zigbee2mqtt_types::vendors::philips::Zigbee929001953101; // this is broke :(

#[derive(Serialize, Deserialize, Debug)]
struct Light {
    #[serde(rename = "state")]
    state: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Action {
    #[serde(rename = "Action")]
    action: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct ShellyResult {
    #[serde(rename = "Switch1")]
    switch1: Option<Action>,

    #[serde(rename = "Switch2")]
    switch2: Option<Action>,
}

#[derive(Debug, PartialEq, Clone, Serialize)]
pub enum LightState {
    On,
    Off,
    Unknown,
}

impl From<ZigbeeState> for LightState {
    fn from(item: ZigbeeState) -> Self {
        match item {
            ZigbeeState::On => LightState::On,
            ZigbeeState::Off => LightState::Off,
        }
    }
}
impl From<&ZigbeeState> for LightState {
    fn from(item: &ZigbeeState) -> Self {
        match item {
            ZigbeeState::On => LightState::On,
            ZigbeeState::Off => LightState::Off,
        }
    }
}

// All LightStates will be in a mutex
// pub trait AsyncLightState<T> {
//     fn clone_light_state(&self) -> Result<LightState, PoisonError<MutexGuard<T>>>;
//     fn update_light_state(&self, new_state: LightState) -> Result<(), PoisonError<MutexGuard<T>>>;
// }

pub trait HasOverallLightState {
    fn clone_light_state(&self) -> LightState;
    fn update_light_state(&mut self, new_state: LightState);
}

// const SHELLY2: &str = "shelly/sh2";
// const SHELLY_RESULT: &str = "stat/RESULT";
pub const SHELLY_RULE_TIMER: &str = "cmnd/RuleTimer1";

pub async fn handle_light_switch_toggle_publish<L>(
    publish: Publish,
    derived_state: Arc<Mutex<L>>,
    light_endpoints: &[&'static str],
) -> Vec<rumqttc::Publish>
where
    L: HasOverallLightState + std::marker::Send + 'static + HasLatestShellyToggle,
{
    let mut packets = vec![];
    let parsed: ShellyResult =
        match serde_json::from_str(std::str::from_utf8(&publish.payload).unwrap()) {
            Ok(motion_json) => motion_json,
            Err(err) => {
                println!("{:?}", &publish.payload);
                println!("{:?}", err);
                todo!()
            }
        };
    if parsed
        .switch1
        .unwrap_or(Action {
            action: "".to_string(),
        })
        .action
        == "TOGGLE"
        || parsed
            .switch2
            .unwrap_or(Action {
                action: "".to_string(),
            })
            .action
            == "TOGGLE"
    {
        let light_state = derived_state.lock().await.clone_light_state();
        match dbg!(light_state) {
            LightState::Off => {
                packets.append(
                    &mut send_light_state(LightState::On, derived_state, light_endpoints).await,
                );
            }
            LightState::On | LightState::Unknown => {
                packets.append(
                    &mut send_light_state(LightState::Off, derived_state, light_endpoints).await,
                );
            }
        }
    }
    packets
}

async fn send_light_state<L>(
    new_state: LightState,
    derived_state: Arc<Mutex<L>>,
    light_mqtt_endpoints: &[&'static str],
) -> Vec<rumqttc::Publish>
where
    L: HasOverallLightState + HasLatestShellyToggle,
{
    let mut packets = vec![];
    let new_state_string = match new_state {
        LightState::On => "on",
        LightState::Off => "off",
        LightState::Unknown => "off",
    };

    dbg!("turn light");
    for light_endpoint in light_mqtt_endpoints {
        packets.push(rumqttc::Publish::new(
            format!("{}/set", light_endpoint).as_str(),
            QoS::AtLeastOnce,
            new_state_string.to_string().as_bytes(),
        ))
    }

    // todo do I want to update state here?
    derived_state
        .lock()
        .await
        .update_shelly_toggle_state(Instant::now());

    packets
}

pub async fn handle_light_publish_and_update_overall_light_state<G>(
    publish: Publish,
    global_state: Arc<Mutex<G>>,
    shelly_mqtt_endpoint: &str,
) -> Vec<rumqttc::Publish>
where
    G: HasOverallLightState
        + Send
        + 'static
        + HasLatestShellyToggle
        + HasShellyRuleTimerInstant
        + HasLightsStateHashMap,
{
    let mut packets = vec![];
    trace!("publish: {:?}", &publish);
    match update_light_and_return_if_light_heart_beat(&publish, global_state.clone()).await {
        Ok(is_heart_beat) => {
            if is_heart_beat {
                return packets;
            }
        }
        Err(err) => {
            log::error!("no entry in hashmap {}", err);
            return packets;
        }
    }

    let parsed: LightResult =
        match serde_json::from_str(std::str::from_utf8(&publish.payload).unwrap()) {
            Ok(motion_json) => motion_json,
            Err(err) => {
                println!("{:?}", &publish.payload);
                println!("{:?}", err);
                todo!()
            }
        };
    match parsed.state {
        ZigbeeState::On => {
            global_state.lock().await.update_light_state(LightState::On);
        }
        ZigbeeState::Off => {
            global_state
                .lock()
                .await
                .update_light_state(LightState::Off);
        }
    }
    let shelly_topic = format!("{}/{}", shelly_mqtt_endpoint, SHELLY_RULE_TIMER);

    let last_shelly_toggle_message = global_state.lock().await.clone_shelly_toggle_state();
    let last_rule_timer_message = global_state.lock().await.clone_shelly_rule_timer_instant();

    if last_shelly_toggle_message.elapsed().as_secs() < 1
        && last_rule_timer_message
            .checked_duration_since(dbg!(last_shelly_toggle_message))
            .is_none()
    {
        packets.push(rumqttc::Publish::new(shelly_topic, QoS::AtLeastOnce, *b"0"));
        global_state
            .lock()
            .await
            .update_shelly_rule_timer_instant(Instant::now());
    } else {
        dbg!(last_shelly_toggle_message.elapsed());
        dbg!(dbg!(last_rule_timer_message).checked_duration_since(last_shelly_toggle_message));
    }

    packets
}

// todo debate if its a good idea to update the state in this check?
pub async fn update_light_and_return_if_light_heart_beat<G>(
    publish: &Publish,
    global_state: Arc<Mutex<G>>,
) -> Result<bool, NoEntryInHashMap>
where
    G: HasLightsStateHashMap,
{
    let topic = publish.topic.as_str().to_owned();

    let parsed: LightResult = match serde_json::from_str(from_utf8(&publish.payload).unwrap()) {
        Ok(light_json) => light_json,
        Err(err) => {
            error!("{:?}", &publish.payload);
            error!("{:?}", err);
            todo!()
        }
    };
    let last_state = global_state
        .lock()
        .await
        .clone_latest_light_state(&topic)
        .expect("has light");
    if last_state.state == LightState::from(&parsed.state) {
        // no need to update
        let mut updated_state = last_state.clone();
        updated_state.last_heart_beat = Instant::now();
        global_state
            .lock()
            .await
            .update_latest_light_state(&topic, updated_state)?;
        Ok(true)
    } else {
        global_state.lock().await.update_latest_light_state(
            &topic,
            LatestLightState {
                last_state_change: Instant::now(),
                last_heart_beat: last_state.last_heart_beat, // debatable
                state: match parsed.state {
                    ZigbeeState::On => LightState::On,
                    ZigbeeState::Off => LightState::Off,
                },
                brightness: parsed.brightness,
                color_temp: parsed.color_temp,
                color_hsb: HsColour {
                    hue: parsed.color.hue,
                    saturation: parsed.color.saturation,
                },
            },
        )?;
        Ok(false)
    }
}

pub async fn maintain_state<G>(
    global_state: Arc<Mutex<G>>,
    shelly_mqtt_endpoint: &str,
    shelly_power_endpoint: &str,
    light_endpoints: &[&'static str],
) -> Vec<rumqttc::Publish>
where
    G: HasOverallLightState
        + HasLatestShellyToggle
        + HasShellyRuleTimerInstant
        + HasLightsStateHashMap
        + Send
        + 'static,
{
    let mut packets = vec![];
    info!("maintain state");
    let mqtt_message = match global_state.lock().await.clone_light_state() {
        LightState::On => "on",
        LightState::Off => "off",
        LightState::Unknown => "off",
    };
    packets.push(rumqttc::Publish::new(
        shelly_mqtt_endpoint.to_owned() + "/" + shelly_power_endpoint,
        QoS::AtLeastOnce,
        *b"ON",
    ));

    for light_endpoint in light_endpoints {
        packets.push(rumqttc::Publish::new(
            format!("{}/set", light_endpoint).as_str(),
            QoS::AtLeastOnce,
            mqtt_message.to_string().as_bytes(),
        ));
    }

    packets
}
