use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub enum ZigbeeState {
    #[serde(rename = "ON")]
    On,
    #[serde(rename = "OFF")]
    Off,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ZigbeeColor {
    pub hue: u16,
    pub saturation: u8,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct LightResult {
    pub state: ZigbeeState,
    pub brightness: u8,
    pub color: ZigbeeColor,
    pub color_temp: f32,
}
